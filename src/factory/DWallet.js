/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @memberof $digidoci
 * @name DWallet
 * @description 
 */
.factory('DWallet', function($pluf, $injector, PObjectFactory, PObject) {

    var _paymentcache = new PObjectFactory(function(data) {
	if (!this.DPaymentModel) {
	    this.DPaymentModel = $injector.get('DPaymentModel');
	}
	return new this.DPaymentModel(data);
    });
    
    var _transfercache = new PObjectFactory(function(data) {
	if (!this.DTransferModel) {
	    this.DTransferModel = $injector.get('DTransferModel');
	}
	return new this.DTransferModel(data);
    });

    var dWallet = function() {
	PObject.apply(this, arguments);
    };
    dWallet.prototype = new PObject();

    dWallet.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/wallet/:id'
    }, _paymentcache);

    dWallet.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/wallet/:id'
    }, _paymentcache);

    dWallet.prototype.payments = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/wallet/:id/payment/find'
    }, _paymentcache);

    dWallet.prototype.payment = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/wallet/:id/payment/{id}'
    }, _paymentcache);

    dWallet.prototype.newPayment = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/wallet/:id/payment/new'
    }, _paymentcache);
    
    dWallet.prototype.transfers = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/wallet/:id/transfer/find'
    }, _transfercache);

    dWallet.prototype.transfer = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/wallet/:id/transfer/{id}'
    }, _transfercache);

    dWallet.prototype.newTransfer = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/wallet/:id/transfer/new'
    }, _transfercache);
    return dWallet;
});
