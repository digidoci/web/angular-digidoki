/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DDeviceModel
 * @memberof ngDigidoci
 * @description هر دستگاه می‌تواند شامل چندین مدل باشد که با استفاده از این
 *              ساختار نمایش داده می‌شود. این ساختار می‌تواند رنگهای متفاوت برای
 *              این مدل را مدیریت کرده و امکاناتی برای به روز کردن این مدل ارائه
 *              کند.
 * 
 * @attr {integer} id شناسه مدل را تعیین می‌کند.
 * @attr {string} title عنوان یک مدل را تعیین می‌کند.
 * @attr {string} symbol یک سمبل برا مدل تعیین می‌کند. این سمبل باید توسط
 *       برنامه‌های کاربری تفسیر شده و به صورت یک شکل به کاربران نمایش داده شود.
 * @attr {string} description توضیحات مدل را تعیین می‌کند.
 * @attr {dtime} creation_dtime تاریخ ایجاد دستگاه را تعیین می‌کند.
 * @attr {dtime} modif_dtime تاریخ به روز رسانی دستگاه را تعیین می‌‌کند.
 */
.factory('DDeviceModel', function(
	$injector, $pluf, PObjectFactory, PObject) {

    var _cache = new PObjectFactory(function(data) {
	if (!this.DDeviceColor) {
	    this.DDeviceColor = $injector.get('DDeviceColor');
	}
	return new this.DDeviceColor(data);
    });

    var dDeviceModel = function() {
	PObject.apply(this, arguments);
    };
    dDeviceModel.prototype = new PObject();

    dDeviceModel.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/device/:device/model/:id'
    });

    dDeviceModel.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/device/:device/model/:id'
    });

    dDeviceModel.prototype.colors = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/device/:device/model/:id/color/find'
    }, _cache);

    dDeviceModel.prototype.color = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/device/:device/model/:id/color/{id}'
    }, _cache);

    dDeviceModel.prototype.newColor = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/device/:device/model/:id/color/new'
    }, _cache);
    return dDeviceModel;
});
