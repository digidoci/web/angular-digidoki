/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DProblem
 * @memberof ngDigidoci
 * @description ساختار داده‌ای را تعیین می‌کند که برای یک مشکل در نظر گرفته شده
 *              است. با استفاده از این ساختار قادر به به روز رسانی و یا حذف این
 *              مشکل هستید.
 */
.factory('DProblem', function(PObject, $pluf) {
    var dProblem = function() {
	PObject.apply(this, arguments);
    };
    dProblem.prototype = new PObject();

    /**
     * Delete
     */
    dProblem.prototype.delete = $pluf.createDelete({
	method: 'DELETE',
	url: '/api/digidoci/problem/:id'
    });

    /**
     * Update
     */
    dProblem.prototype.update = $pluf.createUpdate({
	method: 'POST',
	url: '/api/digidoci/problem/:id'
    });
    return dProblem;
});
