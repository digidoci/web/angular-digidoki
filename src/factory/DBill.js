/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')

/**
 * @ngdoc factory
 * @memberof $digidoci
 * @name DBill
 * @description 
 *
 * # Request bill
 * 
 * This is a bill relqted to a request.
 * 
 * @attr {integer} id of the bill
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 * @author hadi<mohammad.hadi.mansouri@dpq.co.ir>
 */
.factory('DBill', function($pluf, $injector, PObjectFactory, PObject) {

    /*
     * Bill items factory
     */
    var _cache = new PObjectFactory(function(data) {
	if (!this.DBillItem) {
	    this.DBillItem = $injector.get('DBillItem');
	}
	return new this.DBillItem(data);
    });

    /**
     * Creates new instance of DBill
     */
    var dBill = function() {
	PObject.apply(this, arguments);
    };
    dBill.prototype = new PObject();

    /**
     * Updates the bill
     */
    dBill.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/bill/:id'
    }, _cache);

    /**
     * Deletes the bill
     */
    dBill.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/bill/:id'
    }, _cache);

    /**
     * Gets items of the bill
     * 
     * returns paginated items of the bill.
     */
    dBill.prototype.items = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/bill/:id/item/find'
    }, _cache);

    /**
     * Gets an item of the bill
     */
    dBill.prototype.item = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/bill/:id/item/{id}',
    }, _cache);

    /**
     * Create new item for the bill
     */
    dBill.prototype.newItem = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/bill/:id/item/new',
    }, _cache);
    return dBill;
});
