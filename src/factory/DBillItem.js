/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')

/**
 * @ngdoc factory
 * @memberof $digidoci
 * @name DBillItem
 * @description 
 *
 * # Item of a bill
 * 
 * Each bill is composition of a several item. This class define an item 
 * of a bill.
 * 
 * @attr {integer} id of the bill item
 * @attr {string} title
 * @attr {string} description
 * @attr {integer} cost
 * @attr {date} creation_dtime
 * @attr {integer} bill
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 * @author hadi<mohammad.hadi.mansouri@dpq.co.ir>
 */
.factory('DBillItem', function($pluf, $injector, PObjectFactory, PObject) {

    /**
     * Creates a new instance of this class
     */
    var dBillItem = function() {
	PObject.apply(this, arguments);
    };

    dBillItem.prototype = new PObject();

    /**
     * Updates the item
     */
    dBillItem.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/bill/:bill/item/:id'
    });

    /**
     * Deletes the item
     */
    dBillItem.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/bill/:bill/item/:id'
    });

    return dBillItem;
});
