/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @memberof ngDigidoci
 * @name DRequest
 * @description مدل داده‌ای برای تعیین یک تقاضا را تعیین می‌کند.
 * 
 * 
 * @date 0.1.6 Bills management is added.
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 * @author hadi<mohammad.hadi.mansouri@dpq.co.ir>
 */
.factory('DRequest', function($http, $pluf, $httpParamSerializerJQLike, $injector, 
	DHistory, PObject, PObjectCache, PObjectFactory) {

    /*
     * Bill factory
     */
    var _billCache = new PObjectFactory(function(data) {
	if (!angular.isDefined(this.DBill)) {
	    this.DBill = $injector.get('DBill');
	}
	return new this.DBill(data);
    });

    /**
     * Creates new instane of request
     */
    var dRequest = function() {
	PObject.apply(this, arguments);
	this._cache = new PObjectCache(function(data){
	    return new DHistory(data);
	});
	var hurl = '/api/digidoci/request/'+this.id +'/history/find';
	if(this.secureId){
	    hurl = '/api/digidoci/request/'+this.secureId +'/history/find';
	}
	this.histories = $pluf.createFind({
	    method : 'GET',
	    url : hurl,
	}, this._cache);
    };
    dRequest.prototype = new PObject();

    /**
     * Runs an action on the request
     * 
     */
    dRequest.prototype.runAction = function(action, data){
	var scope = this;
	return $http({
	    method: 'POST',
	    url: '/api/digidoci/request/'+this.id+ '/' + action,
	    data: $httpParamSerializerJQLike(data),
	    headers : {
		'Content-Type' : 'application/x-www-form-urlencoded'
	    }
	}).then(function(res){
	    scope.setData(res.data);
	    return scope;
	});
    };

    dRequest.prototype.actionProperty = function(action){
	return $http({
	    method: 'GET',
	    url: '/api/digidoci/request/'+this.id+ '/' + action
	});
    };

    /**
     * Set a zone for request
     */
    dRequest.prototype.setZone = function(zone, message){
	return this.runAction('setZone',{
	    zoneId: zone.id,
	    description: message
	});
    };

    /**
     * تعمیرگاه را تعیین می‌کند.
     */
    dRequest.prototype.setWorkshop = function(workshop, message){
	return this.runAction('setWorkshop',{
	    workshopId: workshop.id,
	    description: message
	});
    };

    dRequest.prototype.workshopDelivered = function(message){
	return this.runAction('workshopDelivered',{
	    description: message
	});
    };
    dRequest.prototype.workshopStartFix = function(message){
	return this.runAction('workshopStartFix',{
	    description: message
	});
    };
    dRequest.prototype.workshopNeedTime = function(message){
	return this.runAction('workshopNeedTime',{
	    description: message
	});
    };
    dRequest.prototype.workshopEndFix = function(data){
	return this.runAction('workshopEndFix',{
	    description: data.message,
	    total_cost: data.total_cost,
	    spare_cost: data.spare_cost
	});
    };
    dRequest.prototype.workshopFailFix = function(message){
	return this.runAction('workshopFailFix',{
	    description: message
	});
    };
    dRequest.prototype.workshopGiveBack = function(message){
	return this.runAction('workshopGiveBack',{
	    description: message
	});
    };

    /**
     * Set a fixer
     */
    dRequest.prototype.setFixer = function(user, message){
	return this.runAction('setFixer',{
	    fixerId: user.id,
	    description: message
	});
    };

    dRequest.prototype.incompleteInfo = function(message){
	return this.runAction('incompleteInfo',{
	    description: message
	});
    };

    dRequest.prototype.remoteConsultant = function(message){
	return this.runAction('remoteConsultant',{
	    description: message
	});
    };

    /**
     * Schedule the request
     */
    dRequest.prototype.setSchedule = function(location, time, message){
	return this.runAction('schedule',{
	    location: location,
	    time: time,
	    description: message
	});
    };

    /**
     * Fix the request
     */
    dRequest.prototype.fix = function(data){
	return this.runAction('fix',{
	    description: data.message,
	    total_cost: data.total_cost,
	    spare_cost: data.spare_cost
	});
    };
    dRequest.prototype.impossibleFix = function(message){
	return this.runAction('impossibleFix',{
	    description: message
	});
    };

    /**
     * Close the request
     */
    dRequest.prototype.close = function(message){
	return this.runAction('close',{
	    description: message
	});
    };

    /**
     * Archive the request
     */
    dRequest.prototype.archive = function(message){
	return this.runAction('archive',{
	    description: message
	});
    };

    dRequest.prototype.reopen = function(message){
	return this.runAction('reopen',{
	    description: message
	});
    };

    dRequest.prototype.report = function(message){
	return this.runAction('report',{
	    description: message
	});
    };

//  fail()
//  impossible()

    /**
     * 
     */
    dRequest.prototype.update = function(){
	// خصوصیت‌ها باید تعیین شود
    };

    /**
     * تقاضا را از سیستم حذف می‌کند.
     */
    dRequest.prototype.delete = function(){
	var scope = this;
	return $http({
	    method: 'DELETE',
	    url: '/api/digidoci/request/'+this.id
	}).then(function() {
	    scope.id = null;
	    return scope;
	});
    };

    /**
     * دستگاهی را تعیین می‌کند که این تقاضا در مورد آن است.
     * 
     * @memberof DRequest
     * @return {[type]} [description]
     */
    dRequest.prototype.getDevice = function() {
	return this.digidoci.device(this.device);
    };

    // XXX: maso, 1395: این تابع‌ها رو برای کارها نیاز داریم
    dRequest.prototype.getModel = function() {
    };
    dRequest.prototype.getColor = function() {
    };
    dRequest.prototype.getRepairman = function() {
    };

    /*
     * Bills management
     */
    /**
     * Create new bill for the request
     */
    dRequest.prototype.newBill = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/request/:id/bill/new',
    }, _billCache);

    /**
     * List bills of the request
     */
    dRequest.prototype.bills = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/request/:id/bill/find',
    }, _billCache);

    /**
     * Get a bill of the request
     */
    dRequest.prototype.bill = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/request/:id/bill/{id}',
    }, _billCache);

    return dRequest;
});
