/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DDeviceColor
 * @memberof ngDigidoci
 * @description مدل داده‌ای برای مدیریت رنگها را ایجاد می‌کند. برای هر مدل از
 *              دستگاه رنگ‌های متفاوتی در نظر گرفته می‌شود. هر یک از این رنگها
 *              با استفاده از این ساختار مدیریت می‌شود.
 */
.factory('DDeviceColor', function($pluf, PObject) {
    var dDeviceColor = function() {
	PObject.apply(this, arguments);
    };
    dDeviceColor.prototype = new PObject();

    dDeviceColor.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/device/:device/model/:device_model/color/:id'
    });

    dDeviceColor.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/device/:device/model/:device_model/color/:id'
    });

    return dDeviceColor;
});
