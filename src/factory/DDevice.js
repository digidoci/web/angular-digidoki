/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @memberof $digidoci
 * @name DDevice
 * @description مدل داده‌ای یک گوشی را ایجاد می‌کند. این مدل داده‌ای برای کار با
 *              گوشی، به روز کردن و یا حذف آن از سیستم استفاده می‌شود.
 * 
 * @attr {integer} id شناسه دستگاه در سرور را تعیین می‌کند.
 * @attr {string} title عنوان دستگاه را تعیین می کند
 * @attr {string} symbol یک سمبل را برای دستگاه تعیین می‌کند. این سمبل با
 *       استفاده از تکنیک‌های موجود در برنامه‌های کاربری باید به شکل‌های قابل
 *       نمایش برای کاربران تبدیل شود.
 * @attr {string} description یک توضیح در مورد دستگاه است.
 * @attr {dtime} creation_dtime تاریخ ایجاد دستگاه را تعیین می‌کند.
 * @attr {dtime} modif_dtime تاریخ به روز رسانی دستگاه را تعیین می‌‌کند.
 */
.factory('DDevice', function($pluf, $injector, PObjectFactory, PObject) {

    var _cache = new PObjectFactory(function(data) {
	if (!this.DDeviceModel) {
	    this.DDeviceModel = $injector.get('DDeviceModel');
	}
	return new this.DDeviceModel(data);
    });

    var dDevice = function() {
	PObject.apply(this, arguments);
    };
    dDevice.prototype = new PObject();

    dDevice.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/device/:id'
    }, _cache);

    dDevice.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/device/:id'
    }, _cache);

    dDevice.prototype.models = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/device/:id/model/find'
    }, _cache);

    dDevice.prototype.model = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/device/:id/model/{id}'
    }, _cache);

    dDevice.prototype.newModel = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/device/:id/model/new'
    }, _cache);
    return dDevice;
});
