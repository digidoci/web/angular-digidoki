/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DTransferModel
 * @memberof ngDigidoci
 * @description The function of this factory are not usable. Because delete or update of a transfer is not possible.
 * 
 */
.factory('DTransferModel', function($pluf, PObject) {
    var DTransferModel = function() {
	PObject.apply(this, arguments);
    };
    DTransferModel.prototype = new PObject();

    DTransferModel.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/wallet/:wallet/transfer/:id'
    });

    DTransferModel.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/wallet/:wallet/transfer/:id'
    });

    return DTransferModel;
});
