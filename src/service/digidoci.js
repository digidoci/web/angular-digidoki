/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngDigiDoki')
/**
 * @ngdoc service
 * @memberof ngDigiDoki
 * @description DigiDoci Service
 *  # DigiDoci Service
 * 
 * این سرویس تمام دستکاری‌های اولیه سیستم را فراهم می‌‌کند. تمام فراخوانی‌هایی
 * که بخواهد با موجودیت‌های دیجی‌دکی کار کند باید از این سرویش کار را شروع کند.
 * برای نمونه گرفته فهرست دستگاه‌ها مورد حمایت به صورت زیر هست:
 * 
 * 
 * <pre><code>
 * $digidoci.devices(p).then(function(devicePage) {
 *     // Do something
 *     });
 * </code></pre>
 * 
 * در این سرویس موجودیت‌های پایه مدیریت می‌شوند که عبارتند از: - DDevice -
 * DProblem - DNet - DRequest
 * 
 * 
 * @date 0.1.6 Bills management is added.
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 * @author hadi<mohammad.hadi.mansouri@dpq.co.ir>
 */
.service('$digidoci',function($pluf, PObjectCache, DProblem, 
	DDevice, DNet, DRequest, PObjectFactory, DZone, DWorkshop, DBill, DWallet) {

    /*
     * کار با دستگاه‌هایی که در سیستم کش شده است.
     */
    var _deviceCache = new PObjectCache(function(data) {
	return new DDevice(data);
    });
    var _problemCache = new PObjectCache(function(data) {
	return new DProblem(data);
    });
    var _netCache = new PObjectCache(function(data) {
	return new DNet(data);
    });
    var _requestCache = new PObjectFactory(function(data) {
	return new DRequest(data);
    });
    var _zoneCache = new PObjectCache(function(data) {
	return new DZone(data);
    });
    var _workshopCache = new PObjectCache(function(data) {
	return new DWorkshop(data);
    });

    var _billCache = new PObjectCache(function(data) {
	return new DBill(data);
    });
    
    var _walletCache = new PObjectCache(function(data) {
	return new DWallet(data);
    });

    /**
     * فهرستی از تمام دستگاه‌های مورد حمایت تعیین می‌کند. این فهرست به
     * صورت صفحه بندی شده در اختیار کاربران قرار می‌گیرد.
     * 
     * @memberof $digidoci
     * @param {PaginatorParameter}
     *                p تنظیم‌های مورد استفاده در صفحه بندی
     * @return {PaginatorPage<DDevice>} صفحه ایجاد شده
     */
    this.devices = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/device/find'
    }, _deviceCache);

    /**
     * اطلاعات یک دستگاه را تعیین می‌کند. هر دستگاه با استفاده از یک
     * شناسه یکتا تعیین می‌شود که با استفاده از آن می‌تواند اطلاعات آن
     * را بازیابی کرد.
     * 
     * @memberof $digidoci
     * @param {Integer}
     *                id شناسه دستگاه
     * @return {DDevice} اطلاعات دستگاه
     */
    this.device = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/device/{id}'
    }, _deviceCache);

    /**
     * یک دستگاه جدید ایجاد می‌کند.
     * 
     * @memberof $digidoci
     * @param Object
     *                خصوصیت‌های مورد نیاز برای ایجاد دستگاه
     */
    this.newDevice = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/device/new'
    }, _deviceCache);

    /**
     * فهرست تمام مشکلات را تعیین می‌کند
     */
    this.problems = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/problem/find'
    }, _problemCache);

    /**
     * گرفتن یک مشکل از سرور
     * 
     * @memberof $digidoci
     */
    this.problem = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/problem/{id}'
    }, _problemCache);

    /**
     * ایجاد یک مشکل جدید
     * 
     * @memberof $digidoci
     */
    this.newProblem = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/problem/new'
    }, _problemCache);

    this.nets = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/network/find',
    }, _netCache);
    this.net = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/network/{id}'
    }, _netCache);
    this.newNet = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/network/new'
    }, _netCache);

    this.requests = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/request/find'
    }, _requestCache);
    this.request = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/request/{id}'
    }, _requestCache);
    this.newRequest = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/request/new'
    }, _requestCache);

    this.zones = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/zone/find'
    }, _zoneCache);
    this.zone = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/zone/{id}'
    }, _zoneCache);
    this.newZone = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/zone/new'
    }, _zoneCache);

    this.workshops = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/workshop/find'
    }, _workshopCache);
    this.workshop = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/workshop/{id}'
    }, _workshopCache);
    this.newWorkshop = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/workshop/new'
    }, _workshopCache);

    /*
     * Bill management
     */
    /**
     * Finds list of bills
     */
    this.bills = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/bill/find'
    }, _billCache);

    /**
     * Gets a bill
     */
    this.bill = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/bill/{id}'
    }, _billCache);

    /**
     * Creates new bill
     */
    this.newBill = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/bill/new'
    }, _billCache);
    
    /**
     * فهرست تمام مشکلات را تعیین می‌کند
     */
    this.wallets = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/wallet/find'
    }, _walletCache);
    
     /**
     * Get a wallet
     */
    this.wallet = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/wallet/{id}'
    }, _walletCache);
    
    /**
     * Creates new wallet
     */
    this.newWallet = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/wallet/new'
    }, _billCache);
});
