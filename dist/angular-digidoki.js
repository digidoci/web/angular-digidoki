/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * @ngdoc module
 * @name ngDigidoci
 * @description
 * 
 * این ماژول تمام ابزارها و مدلهای مورد نیاز برای ارتباط با سرورهای دیجی دکی را
 * پیاده سازی کرده است. برای استفاده از ابزارهای ارائه شده تنها کافی است که این
 * ماژول را به عنوان وابستگی به پروژه خود اضافه کنید.
 * 
 * @see http://www.digidoki.com
 */
angular.module('ngDigiDoki', [ 'pluf' ]);

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')

/**
 * @ngdoc factory
 * @memberof $digidoci
 * @name DBill
 * @description 
 *
 * # Request bill
 * 
 * This is a bill relqted to a request.
 * 
 * @attr {integer} id of the bill
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 * @author hadi<mohammad.hadi.mansouri@dpq.co.ir>
 */
.factory('DBill', function($pluf, $injector, PObjectFactory, PObject) {

    /*
     * Bill items factory
     */
    var _cache = new PObjectFactory(function(data) {
	if (!this.DBillItem) {
	    this.DBillItem = $injector.get('DBillItem');
	}
	return new this.DBillItem(data);
    });

    /**
     * Creates new instance of DBill
     */
    var dBill = function() {
	PObject.apply(this, arguments);
    };
    dBill.prototype = new PObject();

    /**
     * Updates the bill
     */
    dBill.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/bill/:id'
    }, _cache);

    /**
     * Deletes the bill
     */
    dBill.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/bill/:id'
    }, _cache);

    /**
     * Gets items of the bill
     * 
     * returns paginated items of the bill.
     */
    dBill.prototype.items = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/bill/:id/item/find'
    }, _cache);

    /**
     * Gets an item of the bill
     */
    dBill.prototype.item = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/bill/:id/item/{id}',
    }, _cache);

    /**
     * Create new item for the bill
     */
    dBill.prototype.newItem = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/bill/:id/item/new',
    }, _cache);
    return dBill;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')

/**
 * @ngdoc factory
 * @memberof $digidoci
 * @name DBillItem
 * @description 
 *
 * # Item of a bill
 * 
 * Each bill is composition of a several item. This class define an item 
 * of a bill.
 * 
 * @attr {integer} id of the bill item
 * @attr {string} title
 * @attr {string} description
 * @attr {integer} cost
 * @attr {date} creation_dtime
 * @attr {integer} bill
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 * @author hadi<mohammad.hadi.mansouri@dpq.co.ir>
 */
.factory('DBillItem', function($pluf, $injector, PObjectFactory, PObject) {

    /**
     * Creates a new instance of this class
     */
    var dBillItem = function() {
	PObject.apply(this, arguments);
    };

    dBillItem.prototype = new PObject();

    /**
     * Updates the item
     */
    dBillItem.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/bill/:bill/item/:id'
    });

    /**
     * Deletes the item
     */
    dBillItem.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/bill/:bill/item/:id'
    });

    return dBillItem;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @memberof $digidoci
 * @name DDevice
 * @description مدل داده‌ای یک گوشی را ایجاد می‌کند. این مدل داده‌ای برای کار با
 *              گوشی، به روز کردن و یا حذف آن از سیستم استفاده می‌شود.
 * 
 * @attr {integer} id شناسه دستگاه در سرور را تعیین می‌کند.
 * @attr {string} title عنوان دستگاه را تعیین می کند
 * @attr {string} symbol یک سمبل را برای دستگاه تعیین می‌کند. این سمبل با
 *       استفاده از تکنیک‌های موجود در برنامه‌های کاربری باید به شکل‌های قابل
 *       نمایش برای کاربران تبدیل شود.
 * @attr {string} description یک توضیح در مورد دستگاه است.
 * @attr {dtime} creation_dtime تاریخ ایجاد دستگاه را تعیین می‌کند.
 * @attr {dtime} modif_dtime تاریخ به روز رسانی دستگاه را تعیین می‌‌کند.
 */
.factory('DDevice', function($pluf, $injector, PObjectFactory, PObject) {

    var _cache = new PObjectFactory(function(data) {
	if (!this.DDeviceModel) {
	    this.DDeviceModel = $injector.get('DDeviceModel');
	}
	return new this.DDeviceModel(data);
    });

    var dDevice = function() {
	PObject.apply(this, arguments);
    };
    dDevice.prototype = new PObject();

    dDevice.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/device/:id'
    }, _cache);

    dDevice.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/device/:id'
    }, _cache);

    dDevice.prototype.models = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/device/:id/model/find'
    }, _cache);

    dDevice.prototype.model = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/device/:id/model/{id}'
    }, _cache);

    dDevice.prototype.newModel = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/device/:id/model/new'
    }, _cache);
    return dDevice;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DDeviceColor
 * @memberof ngDigidoci
 * @description مدل داده‌ای برای مدیریت رنگها را ایجاد می‌کند. برای هر مدل از
 *              دستگاه رنگ‌های متفاوتی در نظر گرفته می‌شود. هر یک از این رنگها
 *              با استفاده از این ساختار مدیریت می‌شود.
 */
.factory('DDeviceColor', function($pluf, PObject) {
    var dDeviceColor = function() {
	PObject.apply(this, arguments);
    };
    dDeviceColor.prototype = new PObject();

    dDeviceColor.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/device/:device/model/:device_model/color/:id'
    });

    dDeviceColor.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/device/:device/model/:device_model/color/:id'
    });

    return dDeviceColor;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DDeviceModel
 * @memberof ngDigidoci
 * @description هر دستگاه می‌تواند شامل چندین مدل باشد که با استفاده از این
 *              ساختار نمایش داده می‌شود. این ساختار می‌تواند رنگهای متفاوت برای
 *              این مدل را مدیریت کرده و امکاناتی برای به روز کردن این مدل ارائه
 *              کند.
 * 
 * @attr {integer} id شناسه مدل را تعیین می‌کند.
 * @attr {string} title عنوان یک مدل را تعیین می‌کند.
 * @attr {string} symbol یک سمبل برا مدل تعیین می‌کند. این سمبل باید توسط
 *       برنامه‌های کاربری تفسیر شده و به صورت یک شکل به کاربران نمایش داده شود.
 * @attr {string} description توضیحات مدل را تعیین می‌کند.
 * @attr {dtime} creation_dtime تاریخ ایجاد دستگاه را تعیین می‌کند.
 * @attr {dtime} modif_dtime تاریخ به روز رسانی دستگاه را تعیین می‌‌کند.
 */
.factory('DDeviceModel', function(
	$injector, $pluf, PObjectFactory, PObject) {

    var _cache = new PObjectFactory(function(data) {
	if (!this.DDeviceColor) {
	    this.DDeviceColor = $injector.get('DDeviceColor');
	}
	return new this.DDeviceColor(data);
    });

    var dDeviceModel = function() {
	PObject.apply(this, arguments);
    };
    dDeviceModel.prototype = new PObject();

    dDeviceModel.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/device/:device/model/:id'
    });

    dDeviceModel.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/device/:device/model/:id'
    });

    dDeviceModel.prototype.colors = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/device/:device/model/:id/color/find'
    }, _cache);

    dDeviceModel.prototype.color = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/device/:device/model/:id/color/{id}'
    }, _cache);

    dDeviceModel.prototype.newColor = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/device/:device/model/:id/color/new'
    }, _cache);
    return dDeviceModel;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DHistory
 * @memberof ngDigidoci
 * @description
 * هر رویدادی که روی یک تقاضا انجام می‌شود با استفاده از یک ساختار داده‌ای در پایگاه داده ذخیره
 * سازی می‌شود. این مدل داده‌ای برای نگهداری این رویدادها پیاده سازی شده است.
 */
.factory('DHistory', function(PObject) {
  var dHistory = function() {
    PObject.apply(this, arguments);
  };
  dHistory.prototype = new PObject();
  return dHistory;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DNet
 * @memberof $digidoci
 * @description
 * مدل داده‌ای و سرویس کار با شبکه‌ها را پیاده سازی می‌کند. با این مدل داده می‌توانید یک شبکه
 * را به روز و یا اینکه آن را از سیستم حذف کنید. 
 */
.factory('DNet', function(PObject, $pluf) {
    var dNet = function() {
	PObject.apply(this, arguments);
    };
    dNet.prototype = new PObject();
    /**
     * Delete
     */
    dNet.prototype.delete = $pluf.createDelete({
	method: 'DELETE',
	url: '/api/digidoci/network/:id'
    });

    /**
     * Update
     */
    dNet.prototype.update = $pluf.createUpdate({
	method: 'POST',
	url: '/api/digidoci/network/:id'
    });
    return dNet;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DPaymentModel
 * @memberof ngDigidoci
 * @description 
 */
.factory('DPaymentModel', function($pluf, PObject) {
    var DPaymentModel = function() {
	PObject.apply(this, arguments);
    };
    DPaymentModel.prototype = new PObject();

    DPaymentModel.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/wallet/:wallet/payment/:id'
    });

    DPaymentModel.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/wallet/:wallet/payment/:id'
    });

    return DPaymentModel;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DProblem
 * @memberof ngDigidoci
 * @description ساختار داده‌ای را تعیین می‌کند که برای یک مشکل در نظر گرفته شده
 *              است. با استفاده از این ساختار قادر به به روز رسانی و یا حذف این
 *              مشکل هستید.
 */
.factory('DProblem', function(PObject, $pluf) {
    var dProblem = function() {
	PObject.apply(this, arguments);
    };
    dProblem.prototype = new PObject();

    /**
     * Delete
     */
    dProblem.prototype.delete = $pluf.createDelete({
	method: 'DELETE',
	url: '/api/digidoci/problem/:id'
    });

    /**
     * Update
     */
    dProblem.prototype.update = $pluf.createUpdate({
	method: 'POST',
	url: '/api/digidoci/problem/:id'
    });
    return dProblem;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @memberof ngDigidoci
 * @name DRequest
 * @description مدل داده‌ای برای تعیین یک تقاضا را تعیین می‌کند.
 * 
 * 
 * @date 0.1.6 Bills management is added.
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 * @author hadi<mohammad.hadi.mansouri@dpq.co.ir>
 */
.factory('DRequest', function($http, $pluf, $httpParamSerializerJQLike, $injector, 
	DHistory, PObject, PObjectCache, PObjectFactory) {

    /*
     * Bill factory
     */
    var _billCache = new PObjectFactory(function(data) {
	if (!angular.isDefined(this.DBill)) {
	    this.DBill = $injector.get('DBill');
	}
	return new this.DBill(data);
    });

    /**
     * Creates new instane of request
     */
    var dRequest = function() {
	PObject.apply(this, arguments);
	this._cache = new PObjectCache(function(data){
	    return new DHistory(data);
	});
	var hurl = '/api/digidoci/request/'+this.id +'/history/find';
	if(this.secureId){
	    hurl = '/api/digidoci/request/'+this.secureId +'/history/find';
	}
	this.histories = $pluf.createFind({
	    method : 'GET',
	    url : hurl,
	}, this._cache);
    };
    dRequest.prototype = new PObject();

    /**
     * Runs an action on the request
     * 
     */
    dRequest.prototype.runAction = function(action, data){
	var scope = this;
	return $http({
	    method: 'POST',
	    url: '/api/digidoci/request/'+this.id+ '/' + action,
	    data: $httpParamSerializerJQLike(data),
	    headers : {
		'Content-Type' : 'application/x-www-form-urlencoded'
	    }
	}).then(function(res){
	    scope.setData(res.data);
	    return scope;
	});
    };

    dRequest.prototype.actionProperty = function(action){
	return $http({
	    method: 'GET',
	    url: '/api/digidoci/request/'+this.id+ '/' + action
	});
    };

    /**
     * Set a zone for request
     */
    dRequest.prototype.setZone = function(zone, message){
	return this.runAction('setZone',{
	    zoneId: zone.id,
	    description: message
	});
    };

    /**
     * تعمیرگاه را تعیین می‌کند.
     */
    dRequest.prototype.setWorkshop = function(workshop, message){
	return this.runAction('setWorkshop',{
	    workshopId: workshop.id,
	    description: message
	});
    };

    dRequest.prototype.workshopDelivered = function(message){
	return this.runAction('workshopDelivered',{
	    description: message
	});
    };
    dRequest.prototype.workshopStartFix = function(message){
	return this.runAction('workshopStartFix',{
	    description: message
	});
    };
    dRequest.prototype.workshopNeedTime = function(message){
	return this.runAction('workshopNeedTime',{
	    description: message
	});
    };
    dRequest.prototype.workshopEndFix = function(data){
	return this.runAction('workshopEndFix',{
	    description: data.message,
	    total_cost: data.total_cost,
	    spare_cost: data.spare_cost
	});
    };
    dRequest.prototype.workshopFailFix = function(message){
	return this.runAction('workshopFailFix',{
	    description: message
	});
    };
    dRequest.prototype.workshopGiveBack = function(message){
	return this.runAction('workshopGiveBack',{
	    description: message
	});
    };

    /**
     * Set a fixer
     */
    dRequest.prototype.setFixer = function(user, message){
	return this.runAction('setFixer',{
	    fixerId: user.id,
	    description: message
	});
    };

    dRequest.prototype.incompleteInfo = function(message){
	return this.runAction('incompleteInfo',{
	    description: message
	});
    };

    dRequest.prototype.remoteConsultant = function(message){
	return this.runAction('remoteConsultant',{
	    description: message
	});
    };

    /**
     * Schedule the request
     */
    dRequest.prototype.setSchedule = function(location, time, message){
	return this.runAction('schedule',{
	    location: location,
	    time: time,
	    description: message
	});
    };

    /**
     * Fix the request
     */
    dRequest.prototype.fix = function(data){
	return this.runAction('fix',{
	    description: data.message,
	    total_cost: data.total_cost,
	    spare_cost: data.spare_cost
	});
    };
    dRequest.prototype.impossibleFix = function(message){
	return this.runAction('impossibleFix',{
	    description: message
	});
    };

    /**
     * Close the request
     */
    dRequest.prototype.close = function(message){
	return this.runAction('close',{
	    description: message
	});
    };

    /**
     * Archive the request
     */
    dRequest.prototype.archive = function(message){
	return this.runAction('archive',{
	    description: message
	});
    };

    dRequest.prototype.reopen = function(message){
	return this.runAction('reopen',{
	    description: message
	});
    };

    dRequest.prototype.report = function(message){
	return this.runAction('report',{
	    description: message
	});
    };

//  fail()
//  impossible()

    /**
     * 
     */
    dRequest.prototype.update = function(){
	// خصوصیت‌ها باید تعیین شود
    };

    /**
     * تقاضا را از سیستم حذف می‌کند.
     */
    dRequest.prototype.delete = function(){
	var scope = this;
	return $http({
	    method: 'DELETE',
	    url: '/api/digidoci/request/'+this.id
	}).then(function() {
	    scope.id = null;
	    return scope;
	});
    };

    /**
     * دستگاهی را تعیین می‌کند که این تقاضا در مورد آن است.
     * 
     * @memberof DRequest
     * @return {[type]} [description]
     */
    dRequest.prototype.getDevice = function() {
	return this.digidoci.device(this.device);
    };

    // XXX: maso, 1395: این تابع‌ها رو برای کارها نیاز داریم
    dRequest.prototype.getModel = function() {
    };
    dRequest.prototype.getColor = function() {
    };
    dRequest.prototype.getRepairman = function() {
    };

    /*
     * Bills management
     */
    /**
     * Create new bill for the request
     */
    dRequest.prototype.newBill = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/request/:id/bill/new',
    }, _billCache);

    /**
     * List bills of the request
     */
    dRequest.prototype.bills = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/request/:id/bill/find',
    }, _billCache);

    /**
     * Get a bill of the request
     */
    dRequest.prototype.bill = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/request/:id/bill/{id}',
    }, _billCache);

    return dRequest;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DResponse
 * @memberof ngDigidoci
 * @description
 * در ازای هر درخواست کاربر یک جواب در نهایت تولید خواهد شد که با استفاده از این مدل داده‌ای
 * تعیین می‌شود.
 */
.factory('DResponse', function(PObject) {
  var dResponse = function() {
    PObject.apply(this, arguments);
  };
  dResponse.prototype = new PObject();
  return dResponse;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DTransferModel
 * @memberof ngDigidoci
 * @description The function of this factory are not usable. Because delete or update of a transfer is not possible.
 * 
 */
.factory('DTransferModel', function($pluf, PObject) {
    var DTransferModel = function() {
	PObject.apply(this, arguments);
    };
    DTransferModel.prototype = new PObject();

    DTransferModel.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/wallet/:wallet/transfer/:id'
    });

    DTransferModel.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/wallet/:wallet/transfer/:id'
    });

    return DTransferModel;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @memberof $digidoci
 * @name DWallet
 * @description 
 */
.factory('DWallet', function($pluf, $injector, PObjectFactory, PObject) {

    var _paymentcache = new PObjectFactory(function(data) {
	if (!this.DPaymentModel) {
	    this.DPaymentModel = $injector.get('DPaymentModel');
	}
	return new this.DPaymentModel(data);
    });
    
    var _transfercache = new PObjectFactory(function(data) {
	if (!this.DTransferModel) {
	    this.DTransferModel = $injector.get('DTransferModel');
	}
	return new this.DTransferModel(data);
    });

    var dWallet = function() {
	PObject.apply(this, arguments);
    };
    dWallet.prototype = new PObject();

    dWallet.prototype.update = $pluf.createUpdate({
	method : 'POST',
	url : '/api/digidoci/wallet/:id'
    }, _paymentcache);

    dWallet.prototype.delete = $pluf.createDelete({
	method : 'DELETE',
	url : '/api/digidoci/wallet/:id'
    }, _paymentcache);

    dWallet.prototype.payments = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/wallet/:id/payment/find'
    }, _paymentcache);

    dWallet.prototype.payment = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/wallet/:id/payment/{id}'
    }, _paymentcache);

    dWallet.prototype.newPayment = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/wallet/:id/payment/new'
    }, _paymentcache);
    
    dWallet.prototype.transfers = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/wallet/:id/transfer/find'
    }, _transfercache);

    dWallet.prototype.transfer = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/wallet/:id/transfer/{id}'
    }, _transfercache);

    dWallet.prototype.newTransfer = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/wallet/:id/transfer/new'
    }, _transfercache);
    return dWallet;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DNet
 * @memberof $digidoci
 * @description مدل داده‌ای و سرویس کار با شبکه‌ها را پیاده سازی می‌کند. با این
 *              مدل داده می‌توانید یک شبکه را به روز و یا اینکه آن را از سیستم
 *              حذف کنید.
 */
.factory('DZone', function(PObject, $pluf, $http, $injector, PObjectFactory) {
    var _cache = new PObjectFactory(function(data) {
	if (!this.PUser) {
	    this.PUser = $injector.get('PUser');
	}
	return new this.PUser(data);
    });

    var dZone = function() {
	PObject.apply(this, arguments);
    };
    dZone.prototype = new PObject();

    /**
     * Delete zone
     */
    dZone.prototype.delete = $pluf.createDelete({
	method: 'DELETE',
	url: '/api/digidoci/zone/:id'
    });

    /**
     * Update zone
     */
    dZone.prototype.update = $pluf.createUpdate({
	method: 'POST',
	url: '/api/digidoci/zone/:id'
    });
    
    dZone.prototype.members = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/zone/:id/member/find'
    }, _cache);

    dZone.prototype.member = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/zone/:id/member/{id}',
    }, _cache);

    dZone.prototype.newMember = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/zone/:id/member/new',
    }, _cache);
    
    dZone.prototype.deleteUser = function(user) {
	return $http({
	    method : 'DELETE',
	    url : '/api/digidoci/zone/' + this.id + '/member/' + user.id,
	});
    };
    
    
    return dZone;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngDigiDoki')
/**
 * @ngdoc factory
 * @name DNet
 * @memberof $digidoci
 * @description
 * مدل داده‌ای و سرویس کار با شبکه‌ها را پیاده سازی می‌کند. با این مدل داده می‌توانید یک شبکه
 * را به روز و یا اینکه آن را از سیستم حذف کنید. 
 */
.factory('DWorkshop', function(PObject, $pluf) {
    var dWorkshop = function() {
	PObject.apply(this, arguments);
    };
    dWorkshop.prototype = new PObject();
    /**
     * Delete
     */
    dWorkshop.prototype.delete = $pluf.createDelete({
	method: 'DELETE',
	url: '/api/digidoci/workshop/:id'
    });

    /**
     * Update
     */
    dWorkshop.prototype.update = $pluf.createUpdate({
	method: 'POST',
	url: '/api/digidoci/workshop/:id'
    });
    return dWorkshop;
});

/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngDigiDoki')
/**
 * @ngdoc service
 * @memberof ngDigiDoki
 * @description DigiDoci Service
 *  # DigiDoci Service
 * 
 * این سرویس تمام دستکاری‌های اولیه سیستم را فراهم می‌‌کند. تمام فراخوانی‌هایی
 * که بخواهد با موجودیت‌های دیجی‌دکی کار کند باید از این سرویش کار را شروع کند.
 * برای نمونه گرفته فهرست دستگاه‌ها مورد حمایت به صورت زیر هست:
 * 
 * 
 * <pre><code>
 * $digidoci.devices(p).then(function(devicePage) {
 *     // Do something
 *     });
 * </code></pre>
 * 
 * در این سرویس موجودیت‌های پایه مدیریت می‌شوند که عبارتند از: - DDevice -
 * DProblem - DNet - DRequest
 * 
 * 
 * @date 0.1.6 Bills management is added.
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 * @author hadi<mohammad.hadi.mansouri@dpq.co.ir>
 */
.service('$digidoci',function($pluf, PObjectCache, DProblem, 
	DDevice, DNet, DRequest, PObjectFactory, DZone, DWorkshop, DBill, DWallet) {

    /*
     * کار با دستگاه‌هایی که در سیستم کش شده است.
     */
    var _deviceCache = new PObjectCache(function(data) {
	return new DDevice(data);
    });
    var _problemCache = new PObjectCache(function(data) {
	return new DProblem(data);
    });
    var _netCache = new PObjectCache(function(data) {
	return new DNet(data);
    });
    var _requestCache = new PObjectFactory(function(data) {
	return new DRequest(data);
    });
    var _zoneCache = new PObjectCache(function(data) {
	return new DZone(data);
    });
    var _workshopCache = new PObjectCache(function(data) {
	return new DWorkshop(data);
    });

    var _billCache = new PObjectCache(function(data) {
	return new DBill(data);
    });
    
    var _walletCache = new PObjectCache(function(data) {
	return new DWallet(data);
    });

    /**
     * فهرستی از تمام دستگاه‌های مورد حمایت تعیین می‌کند. این فهرست به
     * صورت صفحه بندی شده در اختیار کاربران قرار می‌گیرد.
     * 
     * @memberof $digidoci
     * @param {PaginatorParameter}
     *                p تنظیم‌های مورد استفاده در صفحه بندی
     * @return {PaginatorPage<DDevice>} صفحه ایجاد شده
     */
    this.devices = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/device/find'
    }, _deviceCache);

    /**
     * اطلاعات یک دستگاه را تعیین می‌کند. هر دستگاه با استفاده از یک
     * شناسه یکتا تعیین می‌شود که با استفاده از آن می‌تواند اطلاعات آن
     * را بازیابی کرد.
     * 
     * @memberof $digidoci
     * @param {Integer}
     *                id شناسه دستگاه
     * @return {DDevice} اطلاعات دستگاه
     */
    this.device = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/device/{id}'
    }, _deviceCache);

    /**
     * یک دستگاه جدید ایجاد می‌کند.
     * 
     * @memberof $digidoci
     * @param Object
     *                خصوصیت‌های مورد نیاز برای ایجاد دستگاه
     */
    this.newDevice = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/device/new'
    }, _deviceCache);

    /**
     * فهرست تمام مشکلات را تعیین می‌کند
     */
    this.problems = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/problem/find'
    }, _problemCache);

    /**
     * گرفتن یک مشکل از سرور
     * 
     * @memberof $digidoci
     */
    this.problem = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/problem/{id}'
    }, _problemCache);

    /**
     * ایجاد یک مشکل جدید
     * 
     * @memberof $digidoci
     */
    this.newProblem = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/problem/new'
    }, _problemCache);

    this.nets = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/network/find',
    }, _netCache);
    this.net = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/network/{id}'
    }, _netCache);
    this.newNet = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/network/new'
    }, _netCache);

    this.requests = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/request/find'
    }, _requestCache);
    this.request = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/request/{id}'
    }, _requestCache);
    this.newRequest = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/request/new'
    }, _requestCache);

    this.zones = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/zone/find'
    }, _zoneCache);
    this.zone = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/zone/{id}'
    }, _zoneCache);
    this.newZone = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/zone/new'
    }, _zoneCache);

    this.workshops = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/workshop/find'
    }, _workshopCache);
    this.workshop = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/workshop/{id}'
    }, _workshopCache);
    this.newWorkshop = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/workshop/new'
    }, _workshopCache);

    /*
     * Bill management
     */
    /**
     * Finds list of bills
     */
    this.bills = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/bill/find'
    }, _billCache);

    /**
     * Gets a bill
     */
    this.bill = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/bill/{id}'
    }, _billCache);

    /**
     * Creates new bill
     */
    this.newBill = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/bill/new'
    }, _billCache);
    
    /**
     * فهرست تمام مشکلات را تعیین می‌کند
     */
    this.wallets = $pluf.createFind({
	method : 'GET',
	url : '/api/digidoci/wallet/find'
    }, _walletCache);
    
     /**
     * Get a wallet
     */
    this.wallet = $pluf.createGet({
	method : 'GET',
	url : '/api/digidoci/wallet/{id}'
    }, _walletCache);
    
    /**
     * Creates new wallet
     */
    this.newWallet = $pluf.createNew({
	method : 'POST',
	url : '/api/digidoci/wallet/new'
    }, _billCache);
});
