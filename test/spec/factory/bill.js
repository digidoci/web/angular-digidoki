/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

describe('DBill factory ', function () {
    var DBill;
    var $digidoci;
    var $rootScope;
    var $httpBackend;
    var PaginatorParameter;

    // load the controller's module
    beforeEach(function() {
	module('ngDigiDoki');
    });

    // Initialize the controller and a mock scope
    beforeEach(function (){
	inject(function (_$digidoci_, _$rootScope_, _$httpBackend_,
		_PaginatorParameter_, _DBill_) {
	    $digidoci = _$digidoci_;
	    $rootScope = _$rootScope_;
	    $httpBackend = _$httpBackend_;
	    PaginatorParameter = _PaginatorParameter_;
	    DBill = _DBill_;
	});
    });

    /*
     * Test of API
     */
    it('should consiste of basic functionalities.', function () {
	var object = new DBill();
	// General API
	expect(angular.isFunction(object.delete)).toBe(true);
	expect(angular.isFunction(object.update)).toBe(true);

	// Items API
	expect(angular.isFunction(object.items)).toBe(true);
	expect(angular.isFunction(object.item)).toBe(true);
	expect(angular.isFunction(object.newItem)).toBe(true);
    });

    /*
     * Test of update
     */
    it('should call POST:/api/digidoci/bill/{id} to update a bill.',
	    function(done) {
	var data = {
		id : 1,
		description : 'description',
	};
	var object = new DBill(data);
	object.update()//
	.then(function() {
	    done();
	});
	$httpBackend//
	.expect('POST', '/api/digidoci/bill/1')//
	.respond(200, data);
	expect($httpBackend.flush).not.toThrow();
	$rootScope.$apply();
    });

    /*
     * Test of delete
     */
    it('should call DELETE:/api/digidoci/bill/{id} to remvoe a bill.',
	    function(done) {
	var data = {
		id : 1,
		description : 'description',
	};
	var object = new DBill(data);
	object.delete()//
	.then(function() {
	    done();
	});
	$httpBackend//
	.expect('DELETE', '/api/digidoci/bill/1')//
	.respond(200, data);
	expect($httpBackend.flush).not.toThrow();
	$rootScope.$apply();
    });

    /*
     * Test of find items
     */
    it('should call GET:/api/digidoci/bill/{id}/item/find to list items.',
	    function(done) {
	var data = {
		id : 1,
		description : 'description',
	};
	var object = new DBill(data);
	object.items()//
	.then(function(page) {
	    expect(angular.isDefined(page)).toBe(true);
	    done();
	});
	$httpBackend//
	.expect('GET', '/api/digidoci/bill/1/item/find')//
	.respond(200, {counts:0, items:[]});
	expect($httpBackend.flush).not.toThrow();
	$rootScope.$apply();
    });

    /*
     * Test of create item
     */
    it('should call POST:/api/digidoci/bill/{id}/item/new to create item.',
	    function(done) {
	var data = {
		id : 1,
		description : 'description',
	};
	var object = new DBill(data);
	object.newItem({discription:'example'})//
	.then(function(object) {
	    expect(angular.isDefined(object)).toBe(true);
	    done();
	});
	$httpBackend//
	.expect('POST', '/api/digidoci/bill/1/item/new')//
	.respond(200, {id:1, description:'example'});
	expect($httpBackend.flush).not.toThrow();
	$rootScope.$apply();
    });

    /*
     * Test of get item
     */
    it('should call GET:/api/digidoci/bill/{id}/item/{id} to get an item.',
	    function(done) {
	var data = {
		id : 1,
		description : 'description',
	};
	var object = new DBill(data);
	object.item(1)//
	.then(function(object) {
	    expect(angular.isDefined(object)).toBe(true);
	    done();
	});
	$httpBackend//
	.expect('GET', '/api/digidoci/bill/1/item/1')//
	.respond(200, {id:1, description:'example'});
	expect($httpBackend.flush).not.toThrow();
	$rootScope.$apply();
    });
});
