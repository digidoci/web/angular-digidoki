/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

describe('DBillItem factory ', function () {
    var DBillItem;
    var $digidoci;
    var $rootScope;
    var $httpBackend;
    var PaginatorParameter;

    // load the controller's module
    beforeEach(function() {
	module('ngDigiDoki');
    });

    // Initialize the controller and a mock scope
    beforeEach(function (){
	inject(function (_$digidoci_, _$rootScope_, _$httpBackend_,
		_PaginatorParameter_, _DBillItem_) {
	    $digidoci = _$digidoci_;
	    $rootScope = _$rootScope_;
	    $httpBackend = _$httpBackend_;
	    PaginatorParameter = _PaginatorParameter_;
	    DBillItem = _DBillItem_;
	});
    });

    /*
     * Test of API
     */
    it('should consiste of basic functionalities.', function () {
	var object = new DBillItem();
	expect(angular.isFunction(object.delete)).toBe(true);
	expect(angular.isFunction(object.update)).toBe(true);
    });

    /*
     * Test of update
     */
    it('should call POST:/api/digidoci/bill/{id}/item/{id} to update an item.',
	    function(done) {
	var data = {
		id : 1,
		title : 'device 1',
		bill : 1,
		description : 'description',
	};
	var object = new DBillItem(data);
	object.update()//
	.then(function() {
	    done();
	});
	$httpBackend//
	.expect('POST', '/api/digidoci/bill/1/item/1')//
	.respond(200, data);
	expect($httpBackend.flush).not.toThrow();
	$rootScope.$apply();
    });

    /*
     * Test of delete
     */
    it('should call DELETE:/api/digidoci/bill/{id}/item/{id} to remvoe an item.',
	    function(done) {
	var data = {
		id : 1,
		title : 'device 1',
		bill : 1,
		description : 'description',
	};
	var object = new DBillItem(data);
	object.delete()//
	.then(function() {
	    done();
	});
	$httpBackend//
	.expect('DELETE', '/api/digidoci/bill/1/item/1')//
	.respond(200, data);
	expect($httpBackend.flush).not.toThrow();
	$rootScope.$apply();
    });
});
