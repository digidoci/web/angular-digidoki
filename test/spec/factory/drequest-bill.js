/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

describe('DRequest', function() {
    var DRequest;
    var $digidoci;
    var $rootScope;
    var $httpBackend;
    var PaginatorParameter;

    // Initialize the controller and a mock scope
    beforeEach(function() {
	module('ngDigiDoki');
	inject(function(_$digidoci_, _$rootScope_, _$httpBackend_,
		_PaginatorParameter_, _DRequest_) {
	    $digidoci = _$digidoci_;
	    $rootScope = _$rootScope_;
	    $httpBackend = _$httpBackend_;
	    PaginatorParameter = _PaginatorParameter_;
	    DRequest = _DRequest_;
	});
    });

    /*
     * Bill apis test
     */
    it('should consiste of basic functionalities of bills.', function() {
	var object = new DRequest({
	    id : 1,
	    discription : 'text request'
	});
	expect(angular.isDefined(object.newBill)).toBe(true);
	expect(angular.isDefined(object.bills)).toBe(true);
	expect(angular.isDefined(object.bill)).toBe(true);
    });
    
    /*
     * Test of bill search
     */
    it('should call GET:/api/digidoci/request/{id}/bill/find to list bills.', function(done) {
	var object = new DRequest({
	    id : 1,
	    discription : 'text request'
	});
	object.bills()//
	.then(function(page) {
	    expect(page).not.toBeNull();
	    expect(page.items).not.toBeNull();
	    expect(angular.isFunction(page.items[0].update)).toBe(true);
	    done();
	});
	$httpBackend//
	.expect('GET', '/api/digidoci/request/1/bill/find')//
	.respond(200, {
	    counts : 1,
	    currentPage : 0,
	    itemsPerPage : 10,
	    pageNumber : 1,
	    items : [ {
		id : 1,
		title : 'device 1',
		description : 'description',
	    } ]
	});
	expect($httpBackend.flush).not.toThrow();
	$rootScope.$apply();
    });
    

    /*
     * Get a bill test.
     */
    it('should call GET:/api/digidoci/request/1/bill/{id} to get a bill.', function(done) {
	var object = new DRequest({
	    id : 1,
	    discription : 'text request'
	});
	object.bill(1)//
	.then(function(object) {
	    expect(object).not.toBeNull();
	    expect(angular.isFunction(object.update)).toBe(true);
	    done();
	});
	$httpBackend//
	.expect('GET', '/api/digidoci/request/1/bill/1')//
	.respond(200, {
	    id : 1,
	    title : 'bill 1',
	    description : 'description',
	});
	expect($httpBackend.flush).not.toThrow();
	$rootScope.$apply();
    });

    /*
     * Test of creating a bill
     */
    it('should call POST:/api/digidoci/request/1/bill/new to create a bill.',
	    function(done) {
	var data = {
		id : 1,
		title : 'bill 1',
		description : 'test description',
	};
	var object = new DRequest({
	    id : 1,
	    discription : 'text request'
	});
	object.newBill(data)//
	.then(function(object) {
	    expect(object).not.toBeNull();
	    expect(angular.isFunction(object.update)).toBe(true);
	    done();
	});
	$httpBackend//
	.expect('POST', '/api/digidoci/request/1/bill/new')//
	.respond(200, data);
	expect($httpBackend.flush).not.toThrow();
	$rootScope.$apply();
    });
});
