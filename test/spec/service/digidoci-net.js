/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

describe('Service: $digidoci(Net)', function() {
	var $digidoci;
	var $rootScope;
	var $httpBackend;

	// Initialize the controller and a mock scope
	beforeEach(function() {
		module('ngDigiDoki');
		inject(function(_$digidoci_, _$rootScope_, _$httpBackend_) {
			$digidoci = _$digidoci_;
			$rootScope = _$rootScope_;
			// $timeout = _$timeout_;
			$httpBackend = _$httpBackend_;
		});
	});

	it('should consiste of basic functionalities.', function() {
		expect(angular.isFunction($digidoci.nets)).toBe(true);
		expect(angular.isFunction($digidoci.net)).toBe(true);
		expect(angular.isFunction($digidoci.newNet)).toBe(true);
	});

	it('should call GET:/api/digidoci/network/find list rest.', function(done) {
		$digidoci.nets()//
		.then(function(page) {
			expect(page).not.toBeNull();
			expect(page.items).not.toBeNull();
			expect(angular.isFunction(page.items[0].update)).toBe(true);
			done();
		});
		$httpBackend//
		.expect('GET', '/api/digidoci/network/find')//
		.respond(200, {
			counts : 1,
			currentPage : 0,
			itemsPerPage : 10,
			pageNumber : 1,
			items : [ {
				id : 1,
				title : 'net',
			} ]
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});

	it('should call GET:/api/digidoci/network/{id} to get netowrk.', function(
			done) {
		var id = 1;
		$digidoci.net(id)//
		.then(function(object) {
			expect(object).not.toBeNull();
			expect(angular.isFunction(object.update)).toBe(true);
			done();
		});
		$httpBackend//
		.expect('GET', '/api/digidoci/network/' + id)//
		.respond(200, {
			id : 1,
			title : 'device 1',
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});

	it('should call POST:/api/digidoci/network/new to create a network.',
			function(done) {
				var data = {
					id : 1,
					title : 'device 1',
					symbol : 'symbol',
				};
				$digidoci.newNet(data)//
				.then(function(object) {
					expect(object).not.toBeNull();
					expect(angular.isFunction(object.update)).toBe(true);
					done();
				});
				$httpBackend//
				.expect('POST', '/api/digidoci/network/new')//
				.respond(200, data);
				expect($httpBackend.flush).not.toThrow();
				$rootScope.$apply();
			});
});
