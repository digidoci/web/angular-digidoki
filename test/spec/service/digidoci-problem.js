/*
 * Copyright (c) 2015 DigiDoki http://www.digidoki.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

describe('Service: $digidoci(Problem)', function() {
	var $digidoci;
	var $rootScope;
	var $httpBackend;

	// load the controller's module
	beforeEach(function() {
		module('ngDigiDoki');
	});

	// Initialize the controller and a mock scope
	beforeEach(function() {
		inject(function(_$digidoci_, _$rootScope_, _$httpBackend_) {
			$digidoci = _$digidoci_;
			$rootScope = _$rootScope_;
			// $timeout = _$timeout_;
			$httpBackend = _$httpBackend_;
			// PaginatorParameter = _PaginatorParameter_;
		});
	});

	it('should consiste of basic functionalities.', function() {
		expect(angular.isFunction($digidoci.problems)).toBe(true);
		expect(angular.isFunction($digidoci.problem)).toBe(true);
		expect(angular.isFunction($digidoci.newProblem)).toBe(true);
	});

	it('should call GET:/api/digidoci/problem/find to list problems.',
			function(done) {
				$digidoci.problems()//
				.then(
						function(page) {
							expect(page).not.toBeNull();
							expect(page.items).not.toBeNull();
							expect(angular.isFunction(page.items[0].update))
									.toBe(true);
							done();
						});
				$httpBackend//
				.expect('GET', '/api/digidoci/problem/find')//
				.respond(200, {
					counts : 1,
					currentPage : 0,
					itemsPerPage : 10,
					pageNumber : 1,
					items : [ {
						id : 1,
						title : 'device 1',
						symbol : 'symbol',
						description : 'description',
						deleted : 0
					} ]
				});
				expect($httpBackend.flush).not.toThrow();
				$rootScope.$apply();
			});

	it('should call GET:/../problem/{id} to get a problem.', function(done) {
		var id = 1;
		$digidoci.problem(id)//
		.then(function(object) {
			expect(object).not.toBeNull();
			expect(angular.isFunction(object.update)).toBe(true);
			done();
		});
		$httpBackend//
		.expect('GET', '/api/digidoci/problem/' + id)//
		.respond(200, {
			id : id,
			title : 'object 1',
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});

	it('should call POST:/api/digidoci/problem/new to create a problem.',
			function(done) {
				var data = {
					id : 1,
					title : 'problem 1',
				};
				$digidoci//
				.newProblem(data)//
				.then(function(object) {
					expect(object).not.toBeNull();
					expect(angular.isFunction(object.update)).toBe(true);
					done();
				});
				$httpBackend//
				.expect('POST', '/api/digidoci/problem/new')//
				.respond(200, data);
				expect($httpBackend.flush).not.toThrow();
				$rootScope.$apply();
			});

});
